package Threads_Executors_Runnables;

import java.util.concurrent.atomic.AtomicBoolean;

public class Hero {

    final String name;
    final int speed;
    final int defense;
    final int attack;
    final AtomicBoolean fighting = new AtomicBoolean(false);
    int hp = 120;
    int monsters_defeated = 0;
    boolean alive = true;

    public Hero(String name, int speed, int defense, int attack) {
        super();
        this.name = name;
        this.speed = speed;
        this.defense = defense;
        this.attack = attack;
    }

    public synchronized String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDefense() {
        return defense;
    }

    public int getAttack() {
        return attack;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getMonsters_defeated() {
        return monsters_defeated;
    }

    public void setMonsters_defeated(int monsters_defeated) {
        this.monsters_defeated = monsters_defeated;
    }
}