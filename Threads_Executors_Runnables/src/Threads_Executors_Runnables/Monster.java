package Threads_Executors_Runnables;

import java.util.concurrent.atomic.AtomicBoolean;

public class Monster {
    final int max = 8;
    final int min = 4;
    final int range = max - min + 1;
    final int speed = (int) (Math.random() * range) + min;
    final int defense = (int) (Math.random() * range) + min;
    final int attack = (int) (Math.random() * range) + min;
    final AtomicBoolean fighting = new AtomicBoolean(false);
    int hp = 25;
    boolean alive = true;
    String name = "";

    public Monster() {
        super();
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDefense() {
        return defense;
    }

    public int getAttack() {
        return attack;
    }
}