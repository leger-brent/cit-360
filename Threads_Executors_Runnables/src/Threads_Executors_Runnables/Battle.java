package Threads_Executors_Runnables;

import java.util.ArrayList;

//Threads need to implement Runnable
public class Battle implements Runnable {

    /*You can't pass parameters to the run method. Instead, I create local fields and methods here that I can call
     * to pass along the objects that I need to battle. */
    ArrayList<Monster> monsterFighting;
    ArrayList<Monster> monsterDefeated;
    private Hero h;
    private Monster m;

    //methods to pass ArrayLists from Main
    public synchronized void setMonsterFighting(ArrayList<Monster> monsterFighting) {
        this.monsterFighting = monsterFighting;
    }

    public synchronized void setMonsterDefeated(ArrayList<Monster> monsterDefeated) {
        this.monsterDefeated = monsterDefeated;
    }

    //methods to pass Hero and Monster objects from Main
    public synchronized void setH(Hero h) {
        this.h = h;
    }

    public synchronized void setM(Monster m) {
        this.m = m;
    }

    //Method that is called when a new thread is started
    public void run() {
        //fields that are used for damage calculation
        int damage;
        int hp;
        int hSpeedCounter = 0;
        int mSpeedCounter = 0;
        int max = 6;
        int min = 2;
        int range = max - min;
        boolean hAttack = true;
        int round = 1;

        System.out.println(h.getName() + " has begun to fight " + m.getName() + "!\n");

        /*Loop start the Hero and Monster battle. Each loop will determine who will attack and how much HP to deduct
         * from the object being attacked. This loop will continue until the HP of either object drops to zero*/
        while (h.alive && m.alive) {

            /*Determines who attacks and increments the speed counter of the the non-attacker (will allow slower speed
             * to eventually attack)*/
            if ((m.getSpeed() + mSpeedCounter) > (h.getSpeed() + hSpeedCounter)) {
                hAttack = false;
            } else if ((m.getSpeed() + mSpeedCounter < h.getSpeed() + hSpeedCounter)) {
                hAttack = true;
            }

            //battle and damage calculations if the Hero attacks
            if (hAttack) {
                damage = h.getAttack() - m.getDefense() + (int) (Math.random() * range + min);
                hp = m.getHp() - damage;
                m.setHp(hp);
                hSpeedCounter = 0;
                mSpeedCounter = mSpeedCounter + 2;
                if (m.getHp() <= 0) {
                    m.setHp(0);
                    m.setAlive(false);
                    System.out.println(m.getName() + " has been KO'd");
                    h.setMonsters_defeated(h.getMonsters_defeated() + 1);
                    monsterDefeated.add(m);
                    monsterFighting.remove(m);
                }
            }
            //battle and damage calculations if the Monster attacks
            else {
                damage = m.getAttack() - h.getDefense() + (int) ((Math.random() * range) + min);
                hp = h.getHp() - damage;
                h.setHp(hp);
                mSpeedCounter = 0;
                hSpeedCounter = hSpeedCounter + 2;
                if (h.getHp() <= 0) {
                    h.setHp(0);
                    h.setAlive(false);
                    System.out.println(h.getName() + " has been KO'd");
                }
            }
            //increment the round number
            round = round + 1;

            //pauses 1.5 seconds before the next round
            if (h.isAlive() && m.isAlive()) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        //print the battle results to the screen
        System.out.println(h.getName() + "'s HP: " + h.getHp() + "\n" + m.getName() + "'s HP:" + m.getHp() + "");
        System.out.println("The battle is over between " + h.getName() + " and " + m.getName() + ". It lasted " + round + " rounds.\n");

        //marks the objects as not fighting so they are able to be selected again from the main class.
        h.fighting.set(false);
        m.fighting.set(false);
    }
}