package Threads_Executors_Runnables;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    //Function that creates new monster objects, randomly sets their name from a list, and adds them to an ArrayList
    public static void spawn(ArrayList<Monster> m, int h) {
        for (int i = 0; i < h; i++) {
            Monster random = new Monster();
            int nameInt = (int) (Math.random() * 4);
            if (nameInt == 0) {
                random.setName("Theeder");
            } else if (nameInt == 1) {
                random.setName("Barra");
            } else if (nameInt == 2) {
                random.setName("Aluru");
            } else if (nameInt == 3) {
                random.setName("Zenoir");
            }
            m.add(random);
        }
    }

    //Function that picks a random number between 0 and the number passed to it
    public static int index(int i) {
        i = (int) (Math.random() * i);
        return i;
    }

    /*My main function creates two ArrayLists and fills one with Hero objects and the other with monster objects. The
     * program then randomly selects a hero and an monster and battles them. Since we don't want any heroes just
     * standing around, I use threads to randomly select the heroes not fighting and randomly pair them up with a monster
     * that is not fighting either. If a hero finishes their battle with a monster, they are then eligible to fight
     * another monster. This continues until all the monsters are defeated. The battle results are then shown.*/
    public static void main(String[] args) {
        //Create Hero ArrayList
        ArrayList<Hero> heroes = new ArrayList<>();
        //Create ArrayList for monsters that are still able to fight
        ArrayList<Monster> monsterAlive = new ArrayList<>();
        //Create an ArrayList for monsters that have been defeated
        ArrayList<Monster> monsterDefeated = new ArrayList<>();
        //Create an executor to manage and start threads.
        ExecutorService executor = Executors.newFixedThreadPool(3);

        //Create Hero objects
        Hero Vahn = new Hero("Vahn", 6, 5, 20);
        Hero Noa = new Hero("Noa", 9, 3, 15);
        Hero Gala = new Hero("Gala", 3, 6, 26);
        //Add Hero objects to ArrayList
        heroes.add(Vahn);
        heroes.add(Noa);
        heroes.add(Gala);

        //Variables that will help later on select randomly from the ArrayList
        int hIndex;
        int mIndex;

        //Spawn a random number of monsters between 10 and 20
        spawn(monsterAlive, (int) (Math.random() * 10) + 10);


        //The bread and butter of the main function. Sets up and calls the Battle Thread class.
        //Runs while there are still undefeated monsters
        while (monsterAlive.size() > 0) {
            /*Since I'm threading, this part will start before another thread finishes. That means that  monsters that
             * are fighting haven't been defeated yet by the time the while loop starts, but will be in middle of it.
             * Occasionally the monsterAlive ArrayList could hit 0 size and cause an out of bounds error. Placing this
             * a try catch will let me catch those errors and handle them.*/
            try {

                //This do-while loop will find a hero that is not currently fighting and notes their index number.
                do {
                    hIndex = index(heroes.size());
                } while (heroes.get(hIndex).fighting.get());

                //This do-while loop will find a monster that is not currently fighting and notes their index number.
                do {
                    mIndex = index(monsterAlive.size());
                } while (monsterAlive.get(mIndex).fighting.get());

                //marks the hero and the monster atomic variable fighting as true.
                heroes.get(hIndex).fighting.set(true);
                monsterAlive.get(mIndex).fighting.set(true);

                //creates a new battle object
                Battle battle = new Battle();

                //passes the hero object to the battle object
                battle.setH(heroes.get(hIndex));
                //passes the monster object to the battle object
                battle.setM(monsterAlive.get(mIndex));

                //passes the ArrayList for the monsters to the battle object so they can be updated when the thread ends
                battle.setMonsterFighting(monsterAlive);
                battle.setMonsterDefeated(monsterDefeated);

                //start a new thread of the battle object
                executor.execute(battle);


                //pause the loop for .3 seconds before starting another thread
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            /*catches the out of bounds error mentioned earlier. Don't need to print anything because at this point all
             * of the monsters have been defeated and the battle are over.*/ catch (IndexOutOfBoundsException e) {
            }

        }

        //Stops initiating new threads
        executor.shutdown();

        //Waits for all threads to end before moving on to the next part of the program.
        try {
            executor.awaitTermination(24L, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //Print battle stats to the screen
        System.out.println("You have defeated all of the monsters!");
        System.out.println("Final Stats:");
        for (Hero hero : heroes) {
            System.out.println(hero.getName() + " defeated " + hero.getMonsters_defeated() + " monsters!");
        }
        System.out.println(monsterDefeated.size());
    }
}