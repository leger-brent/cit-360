package Servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//Tell the program that we're using a servlet
@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
//Needs to extend the Server class
public class Servlets extends HttpServlet {
    //Need a doPost and a doGet method. Both define the request and response parameters.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //define the text as HTML
        response.setContentType("text/html");

        //create a PrintWriter object
        PrintWriter output = response.getWriter();

        //Creates a string that gets the value of the input name from the HTML file.
        String hero = request.getParameter("hero");

        //Code that will print a different statement dependent on the input
        if (hero.equals("Vahn")) {
            output.println("You have selected " + hero + ", a well-rounded fighter. He uses a combination of power and speed to overcome his opponents.");
        } else if (hero.equals("Noa")) {
            output.println("You have selected " + hero + ". Noa's strength lies in her lightning fast reflexes. What she lacks in muscle, she makes up for with speed.");
        } else {
            output.println("You have selected " + hero + ". Gala overpowers his opponents through brute strength. Calm and collected, he delivers the most powerful blows.");
        }
        //Close the PrintWriter
        output.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter output = response.getWriter();
        String hero = request.getParameter("hero");

        if (hero.equals("Vahn")) {
            output.println("Element: Fire");
        } else if (hero.equals("Noa")) {
            output.println("Element: Wind");
        } else {
            output.println("Element: Lightning");
        }

        output.close();
    }

}
