package JUNIT;


import java.util.ArrayList;
import java.util.Random;

public class Hero {
    String name;
    int hp;
    int str;
    int def;
    int spd;

    public Hero(String name, int hp, int str, int def, int spd) {
        this.name = name;
        this.hp = hp;
        this.str = str;
        this.def = def;
        this.spd = spd;
    }

    public String sayHi() {
        String s;
        s = "Hi, my name is " + this.name + ".";
        return s;
    }

    public int hit() {
        Random r = new Random();
        int rand = r.nextInt(3) + 1;
        this.hp = this.hp - rand;
        return this.hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public int getSpd() {
        return spd;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }
}
