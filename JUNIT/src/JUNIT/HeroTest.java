package JUNIT;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    // This is a function from the Hero class that returns a string of the object stating their name
    void sayHi() {
        //Create the object
        Hero Vahn = new Hero("Vahn", 10, 10, 10, 10);
        //Test that there's an actual object and it isn't null
        assertNotNull(Vahn);
        //Create a string of what the Object is supposed to say.
        String expectedOutput = "Hi, my name is Vahn.";
        //Test the object function against the expected output to see if they're equal
        assertEquals(Vahn.sayHi(), expectedOutput);
    }

    @Test
        // Another function from the hero class where the hero gets injured and their HP changes
    void hit() {
        //Create the object
        Hero h = new Hero("Test", 10, 10, 10, 10);
        //Create a baseline of what the original HP was
        int testHP = h.hp;
        //run the function
        h.hit();
        //verify that the hp is no longer the same
        assertNotEquals(testHP, h.hp);
    }

    @Test
        // A function in the JUNIT class that creates 3 heroes in an ArrayList
    void createThreeHeroes() {
        //create an ArrayList to hold Hero objects
        ArrayList<Hero> h = new ArrayList<Hero>();
        //Call the function
        JUNIT.createThreeHeroes(h);
        //Check to see if there are three elements in the ArrayList
        assertTrue(h.size() == 3);
    }

    @Test
        //Just a test validate data of new created objects. This verifies that each object is being created correctly
    void heroValidate() {
        //create the objects
        Hero v = new Hero("Vahn", 10, 10, 10, 10);
        Hero n = new Hero("Noa", 10, 8, 8, 12);
        //give this object a null value to test with
        Hero g = new Hero(null, 10, 10, 10, 10);

        //create a String to test with
        String testNull = g.getName();
        //Use the setName function to change the value from null
        g.setName("Gala");

        //create a Hero object to compare against
        Hero test = v;

        //Test that v and test are the same
        assertSame(v, test);
        //Test that n and test are different.
        assertNotSame(n, test);
        //verify that null data was actually inserted
        assertNull(testNull);
        //Test that the null data was changed
        assertFalse(g.getName().equals(testNull));
    }

    @Test
    //This test is used to validate that the functions from object are correctly returning the right data and can be stored in an array
    public void heroesArray() {
        //create objects
        Hero v = new Hero("Vahn", 10, 10, 10, 10);
        Hero n = new Hero("Noa", 10, 8, 8, 12);
        Hero g = new Hero("Gala", 10, 12, 12, 8);

        //create string array and pass the object values into it
        String[] heroNames = {v.getName(), n.getName(), g.getName()};
        //create a string array and pass the expected values into it
        String[] expectedOutput = {"Vahn", "Noa", "Gala"};
        //test the array to see if it's correct
        assertArrayEquals(heroNames, expectedOutput);
    }
}