package Collections;
import java.util.*;
public class Collections {

    public static void main(String[] args) {

        //LIST BEGIN ----------------------------------------------=
        /*An ArrayList is a resizable array in Java. An array can hold multiple values in a single variable.
         * ArrayLists allows you to add or remove elements from the array after the program has started.*/

        //Create the ArrayList
        ArrayList<Hero> heroes = new ArrayList<Hero>();

        //Create Hero objects using the class I built
        Hero vahn = new Hero("Vahn");
        Hero noa = new Hero("Noa");
        Hero gala = new Hero("Gala");

        //Add the objects to the ArrayList. Note that I add noa twice
        heroes.add(vahn);
        heroes.add(noa);
        heroes.add(gala);
        heroes.add(noa);

        //Print the entire ArrayList
        for (Hero h : heroes) {
            System.out.println(h);
        }
        System.out.println("\n");

        //Demonstrate you can remove objects from and ArrayList. This wil remove a Noa duplicate from the ArrayList
        heroes.remove(3);

        //Print the entire ArrayList again
        for (Hero h : heroes) {
            System.out.println(h);
        }
        //LIST END ------------------------------------------------

        System.out.println("\n");

        //SET BEGIN -----------------------------------------------
        /*A Set is a collection that only allows one of each unique value.*/

        //Create the Set
        HashSet<String> weapons = new HashSet<>();

        //Add elements to the Set
        weapons.add("Chaos Breaker");
        weapons.add("Golden Claw");
        weapons.add("Great Axe");

        //Add duplicates to the Set
        weapons.add("Chaos Breaker");
        weapons.add("Golden Claw");
        weapons.add("Great Axe");

        /*Print through the Set. Even though two of each elements were added, only one of each will be printed because
         * the Set doesn't store duplicates.*/
        for (String weapon : weapons) System.out.println(weapon);
        //SET END -------------------------------------------------

        System.out.println("\n");

        //MAP BEGIN -----------------------------------------------
        /*A Map is a collection that stores data in a key:value pair. A key can only have one value associated with it,
         * and new values will overwrite old values for a key if used again*/

        //Create descriptions for cities in the world.
        Map<String, String> cities = new HashMap<>();

        //Add cities to the Map
        cities.put("Rim Elm", "This is your hometown");
        cities.put("Hunter's Fountain", "The hunter's gather here for respite.");
        cities.put("Drake Castle", "The kingdom's castle. Named for the line of kings that have lived here for centuries.");
        cities.put("Snowdrift Cave", "A cave in the far northwest of the kingdom. Since it's on the other side of Mt. Rikuroa, no one really visits it.");

        System.out.println("\n");
        //Print the entire Map
        System.out.println("Four places you will visit on your journey are:");
        for (String s : cities.keySet()) {
            System.out.println("Place: " + s + ".\nDescription: " + cities.get(s) + "\n");
        }

        //Replace Drake Castle Value to demonstrate that putting a value with a key will overwrite it.
        cities.put("Drake Castle", "The drawbridge to Drake Castle has been raised. You can no longer enter.");

        //Alternatively you can use replace to replace a value.
        cities.replace("Rim Elm", "Your hometown has been invaded by the Dark Seru.");

        System.out.println("\n");
        System.out.println("Changes have occurred since you started your journey:");
        //Print the entire Map
        for (String s : cities.keySet()) {
            System.out.println("Place: " + s + ".\nDescription: " + cities.get(s) + "\n");
        }
        //MAP END -------------------------------------------------

        System.out.println("\n");

        //QUEUE BEGIN ---------------------------------------------
        /*A Queue is a collection that stores elements in an ordered list and uses First-In-First-Out.
         * This means that new created objects are always added to the bottom and deleted elements are always deleted
         * from the top.*/

        //Create the Queue
        Queue<String> letter = new LinkedList<>();

        //Add elements to the Queue
        letter.add("Hello Mei,");
        letter.add("I am at Byron Monastery and have met a monk named Gala.");
        letter.add("Master Zopu is sending us to West Voz Forest to revive the Genesis Tree.");
        letter.add("Once we finish here, I hope to be able to return to Rim Elm and visit everyone before we continue on our journey.");

        //Poll removes the oldest element in the Queue. In addition, this prints the value of the poll.
        for (int i = 0; i < letter.size(); ) {
            //This dequeues the collection and polls the oldest value added to the queue.
            System.out.println(letter.poll());

            //As it's polled, the queue decreases. This part prints the number of messages left in the queue.
            if (letter.size() != 1) {
                System.out.println("\033[3mThere are " + letter.size() + " messages left in the queue.\033[0m");
            } else {
                System.out.println("\033[3mThere is " + letter.size() + " message left in the queue.\033[0m");
            }
        }
        //QUEUE END -----------------------------------------------

        System.out.println("\n");

        //TREESET BEGIN -------------------------------------------
        /*This is very similar to a set, although it uses natural order to implicitly sort the elements*/

        //Create the TreeSet
        TreeSet<String> inventory = new TreeSet<>();

        /*Add items to the TreeSet. Sorting an inventory alphabetically would be helpful. Note these are not added
         * alphabetically in their order.*/
        inventory.add("Healing Leaf");
        inventory.add("Door of Wind");
        inventory.add("Magic Fruit");
        inventory.add("Healing Fruit");
        inventory.add("Door of Light");
        inventory.add("Miracle Water");

        //Print the entire TreeSet. It will be listed alphabetically.
        for (String s : inventory) {
            System.out.println(s);
        }
        //TREESET END ---------------------------------------------
    }
}