package Hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.Map;

public class Main {

    public static void main(final String[] args) throws Exception {

        //Add Hero Entities to the database
        HibernateCalls.addHero("Vahn", "Rim Elm", "Fire");
        HibernateCalls.addHero("Noa", "Conkram", "Wind");
        HibernateCalls.addHero("Gala", "Byron Monastery", "Lightning");


        System.out.println("\n");

        //Print the heroes from the database.
        HibernateCalls.showHeroes();

        //Delete a HeroEntity object from the database
        HibernateCalls.deleteHero("Gala");

        //Print the heroes from the database to show the removal of the object
        HibernateCalls.showHeroes();

        //Show that you can use standard Java to manipulate objects stored in the database.
        for (int i = 0; i < 5; i++) {
            HibernateCalls.levelUpHero("Vahn");
        }

        //Shows the stats from the database for the hero we just made updates for
        HibernateCalls.showHeroStats("Vahn");

        //Shows that you can create local objects of the Entity class and they're treated just like normal objects that
        //aren't persistent
        HeroEntity h = HibernateCalls.returnHero("Vahn");
        h.setHeroName("Brent");
        h.setHeroHome("Kuna");
        System.out.println(h.getHeroName() + "\n" + h.getHeroLevel() + "\n" + h.getHeroHome() + "\n");

        //Removes all heroes from the database
        HibernateCalls.deleteAllHeroes();

        //Shows all heroes
        HibernateCalls.showHeroes();
    }
}