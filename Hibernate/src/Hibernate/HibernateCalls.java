package Hibernate;

import org.hibernate.Session;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class HibernateCalls {

    public static void showHeroes() {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();
            List heroes = session.createQuery("from Hibernate.HeroEntity").list();
            if (heroes.size() == 0) {
                System.out.println("There are no Heroes in the database.");
            }
            for (Iterator i = heroes.iterator(); i.hasNext(); ) {
                HeroEntity hero = (HeroEntity) i.next();
                System.out.println("Name: " + hero.getHeroName() + " ");
                System.out.println("Home Town: " + hero.getHeroHome() + " ");
                System.out.println("Element : " + hero.getHeroElement() + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static void addHero(String name, String home, String element) {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();

            HeroEntity hero = new HeroEntity();
            hero.setHeroName(name);
            hero.setHeroHome(home);
            hero.setHeroElement(element);
            hero.setHeroLevel(1);
            hero.setHeroHealth(100);
            hero.setHeroMana(20);
            hero.setHeroAttack(10);
            hero.setHeroDefense(10);
            hero.setHeroSpeed(10);
            session.save(hero);
            session.getTransaction().commit();
            System.out.println(name + " has been added to the database!");
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static HeroEntity returnHero(String name) {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();
            HeroEntity hero = session.get(HeroEntity.class, name);
            return hero;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }
    }

    public static void levelUpHero(String name) {
        Session session = HibernateUtils.getSession();
        Random r = new Random();
        try {
            session.getTransaction().begin();
            HeroEntity hero = session.get(HeroEntity.class, name);
            hero.setHeroLevel(hero.getHeroLevel() + 1);
            hero.setHeroHealth(hero.getHeroHealth() + r.nextInt(3) + 5);
            hero.setHeroMana(hero.getHeroMana() + r.nextInt(3) + 3);
            hero.setHeroAttack(hero.getHeroAttack() + r.nextInt(3) + 1);
            hero.setHeroDefense(hero.getHeroDefense() + r.nextInt(3) + 1);
            hero.setHeroSpeed(hero.getHeroSpeed() + r.nextInt(3) + 1);
            session.getTransaction().commit();
            System.out.println(name + " is now level " + hero.getHeroLevel() + ".\n");
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static void showHeroStats(String name) {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();
            HeroEntity hero = session.get(HeroEntity.class, name);
            System.out.println(name + "'s Stats");
            System.out.println("________________________");
            System.out.println("Level: " + hero.getHeroLevel());
            System.out.println("HP: " + hero.getHeroHealth());
            System.out.println("Mana: " + hero.getHeroMana());
            System.out.println("Attack: " + hero.getHeroAttack());
            System.out.println("Defense: " + hero.getHeroDefense());
            System.out.println("Speed: " + hero.getHeroSpeed() + "\n");
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static void deleteHero(String name) {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();
            HeroEntity hero = session.get(HeroEntity.class, name);
            session.delete(hero);
            session.getTransaction().commit();
            System.out.println(name + " has been removed from the database!\n");
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public static void deleteAllHeroes() {
        Session session = HibernateUtils.getSession();
        try {
            session.getTransaction().begin();
            List heroes = session.createQuery("from Hibernate.HeroEntity").list();
            for (Iterator i = heroes.iterator(); i.hasNext(); ) {
                HeroEntity hero = (HeroEntity) i.next();
                session.delete(hero);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

}
