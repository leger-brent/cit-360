package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "hero")
public class HeroEntity {
    private String heroName;
    private String heroHome;
    private String heroElement;
    private int heroLevel;
    private int heroHealth;
    private int heroMana;
    private int heroAttack;
    private int heroDefense;
    private int heroSpeed;

    @Id
    @Column(name = "heroName", nullable = false, length = 45)
    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    @Basic
    @Column(name = "heroHome", nullable = false, length = 45)
    public String getHeroHome() {
        return heroHome;
    }

    public void setHeroHome(String heroHome) {
        this.heroHome = heroHome;
    }

    @Basic
    @Column(name = "heroElement", nullable = false, length = 45)
    public String getHeroElement() {
        return heroElement;
    }

    public void setHeroElement(String heroElement) {
        this.heroElement = heroElement;
    }

    @Basic
    @Column(name = "heroLevel", nullable = false)
    public int getHeroLevel() {
        return heroLevel;
    }

    public void setHeroLevel(int heroLevel) {
        this.heroLevel = heroLevel;
    }

    @Basic
    @Column(name = "heroHealth", nullable = false)
    public int getHeroHealth() {
        return heroHealth;
    }

    public void setHeroHealth(int heroHealth) {
        this.heroHealth = heroHealth;
    }

    @Basic
    @Column(name = "heroMana", nullable = false)
    public int getHeroMana() {
        return heroMana;
    }

    public void setHeroMana(int heroMana) {
        this.heroMana = heroMana;
    }

    @Basic
    @Column(name = "heroAttack", nullable = false)
    public int getHeroAttack() {
        return heroAttack;
    }

    public void setHeroAttack(int heroAttack) {
        this.heroAttack = heroAttack;
    }

    @Basic
    @Column(name = "heroDefense", nullable = false)
    public int getHeroDefense() {
        return heroDefense;
    }

    public void setHeroDefense(int heroDefense) {
        this.heroDefense = heroDefense;
    }

    @Basic
    @Column(name = "heroSpeed", nullable = false)
    public int getHeroSpeed() {
        return heroSpeed;
    }

    public void setHeroSpeed(int heroSpeed) {
        this.heroSpeed = heroSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HeroEntity that = (HeroEntity) o;

        if (heroLevel != that.heroLevel) return false;
        if (heroHealth != that.heroHealth) return false;
        if (heroMana != that.heroMana) return false;
        if (heroAttack != that.heroAttack) return false;
        if (heroDefense != that.heroDefense) return false;
        if (heroSpeed != that.heroSpeed) return false;
        if (heroName != null ? !heroName.equals(that.heroName) : that.heroName != null) return false;
        if (heroHome != null ? !heroHome.equals(that.heroHome) : that.heroHome != null) return false;
        return heroElement != null ? heroElement.equals(that.heroElement) : that.heroElement == null;
    }

}
