<%--
  Created by IntelliJ IDEA.
  User: Brent
  Date: 7/19/2020
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete Account</title>
</head>
<body>
<fieldset>
    <legend>Delete Account:</legend>
    <form action="delete">
        <label for="id">Account ID Number to delete:</label><br>
        <input type="number" id="id" name="id"><br><br>
        <input type="submit" value="Submit"><br><br>
    </form>
</fieldset>

<a href="index.jsp">Home</a><br>
</body>
</html>
