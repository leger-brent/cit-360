<%--
  Created by IntelliJ IDEA.
  User: Brent
  Date: 7/15/2020
  Time: 9:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bank App</title>
</head>
<body>
<form>
    <a href="${pageContext.request.contextPath}/accounts">Show Accounts</a><br>
    <a href="Deposit.jsp">Deposit or Withdraw</a><br>
    <a href="Transfer.jsp">Transfer</a><br>
    <a href="Add.jsp">Add Account</a><br>
    <a href="Delete.jsp">Delete Account</a><br>
</form>
</body>
</html>
