<%--
  Created by IntelliJ IDEA.
  User: Brent
  Date: 7/19/2020
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Transfer</title>
</head>
<body>
<fieldset>
    <legend>Transfer Funds:</legend>
    <form action="transfer">
        <label for="fromid">Transfer From Account ID:</label><br>
        <input type="number" id="fromid" name="fromid"><br>
        <label for="toid">Transfer To Account ID:</label><br>
        <input type="number" id="toid" name="toid"><br>
        <label for="amount">Amount:</label><br>
        <input type="number" id="amount" name="amount"><br><br>
        <input type="submit" value="Submit"><br><br>
    </form>
</fieldset>
</body>
</html>
