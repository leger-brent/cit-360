package Final;

import Final.AccountsDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "deposit", urlPatterns = {"/deposit"})
public class DepositServlet extends HttpServlet {

    AccountsDAO a = AccountsDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();
        String action = request.getParameter("radiogroup");
        String id = request.getParameter("id");
        String amount = request.getParameter("amount");
        long change = Long.parseLong(amount);


        if (a.findAccount(Integer.parseInt(id)) != null) {
            if (action.equals("Deposit")) {
                a.updateAmount(Integer.parseInt(id), change);
                output.println("Funds deposited.<br>");
            } else {
                long currentAmount = a.findAccount(Integer.parseInt(id)).getAccountAmount();
                if (currentAmount - change >= 0) {
                    change = change * -1;
                    a.updateAmount(Integer.parseInt(id), change);
                    output.println("Funds withdrawn.<br>");
                } else {
                    output.println("You do not have enough funds to withdraw that amount.<br>");
                }
            }
        } else {
            output.println("That account does not exist.<br>");
        }
        output.println("<a href=\"index.jsp\">Back</a>");
    }
}
