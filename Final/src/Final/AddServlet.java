package Final;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

@WebServlet(name = "add", urlPatterns = {"/add"})
public class AddServlet extends HttpServlet {

    AccountsDAO a = AccountsDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();
        List<AccountsEntity> allAccounts = a.ViewAllAccounts();
        AccountsEntity account = new AccountsEntity();
        String type = request.getParameter("radiogroup");
        String nickname = request.getParameter("nickname");
        String amount = request.getParameter("amount");
        if (amount == null || amount == "") {
            amount = "0";
        }
        long deposit = (long) Double.parseDouble(amount);
        long rate = 1;
        int last = 1;
        if (allAccounts.size() > 0) {
            AccountsEntity lastAccount = allAccounts.get(allAccounts.size() - 1);
            last = lastAccount.getAccountId() + 1;
        }


        account.setAccountId(last);
        if (nickname == null || nickname == "") {
            nickname = type + " " + last;
        }
        account.setAccountNickname(nickname);
        account.setAccountType(type);
        account.setAccountAmount(deposit);
        if (type.equals("Checking")) {
            rate = 0;
        }
        account.setAccountInterestRate(rate);

        a.addAccount(account);


        output.println("Account has been added.<br>");
        output.println("<a href=\"index.jsp\">Back</a>");
    }
}
