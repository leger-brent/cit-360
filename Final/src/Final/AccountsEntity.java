package Final;

import javax.persistence.*;

@Entity
@Table(name = "accounts", schema = "final")
public class AccountsEntity {
    private int accountId;
    private String accountType;
    private long accountInterestRate;
    private long accountAmount;
    private String accountNickname;

    @Id
    @Column(name = "account_id", nullable = false)
    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "account_type", nullable = false, length = 45)
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Basic
    @Column(name = "account_interest_rate", nullable = false, precision = 0)
    public long getAccountInterestRate() {
        return accountInterestRate;
    }

    public void setAccountInterestRate(long accountInterestRate) {
        this.accountInterestRate = accountInterestRate;
    }

    @Basic
    @Column(name = "account_amount", nullable = false, precision = 0)
    public long getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(long accountAmount) {
        this.accountAmount = accountAmount;
    }

    @Basic
    @Column(name = "account_nickname", nullable = true, length = 45)
    public String getAccountNickname() {
        return accountNickname;
    }

    public void setAccountNickname(String accountNickname) {
        this.accountNickname = accountNickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountsEntity that = (AccountsEntity) o;

        if (accountId != that.accountId) return false;
        if (accountInterestRate != that.accountInterestRate) return false;
        if (accountAmount != that.accountAmount) return false;
        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        return accountNickname != null ? accountNickname.equals(that.accountNickname) : that.accountNickname == null;
    }

    @Override
    public int hashCode() {
        int result = accountId;
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        result = 31 * result + (int) (accountInterestRate ^ (accountInterestRate >>> 32));
        result = 31 * result + (int) (accountAmount ^ (accountAmount >>> 32));
        result = 31 * result + (accountNickname != null ? accountNickname.hashCode() : 0);
        return result;
    }
}
