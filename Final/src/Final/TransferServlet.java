package Final;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "transfer", urlPatterns = {"/transfer"})
public class TransferServlet extends HttpServlet {

    AccountsDAO a = AccountsDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();

        String fromID = request.getParameter("fromid");
        String toID = request.getParameter("toid");
        String amount = request.getParameter("amount");
        long change = Long.parseLong(amount);

        if (a.findAccount(Integer.parseInt(fromID)) != null && a.findAccount(Integer.parseInt(toID)) != null) {
            long currentAmount = a.findAccount(Integer.parseInt(fromID)).getAccountAmount();
            if (currentAmount - change < 0) {
                output.println(currentAmount);
                output.println("You do not have enough funds to make this transfer.<br>");
            } else {
                a.updateAmount(Integer.parseInt(toID), change);
                change = change * -1;
                a.updateAmount(Integer.parseInt(fromID), change);
                output.println("Funds have been transferred.<br>");
            }
        } else if (a.findAccount(Integer.parseInt(fromID)) == null) {
            output.println("From Account ID is invalid.<br>");
        } else {
            output.println("To Account ID is invalid.<br>");
        }
        output.println("<a href=\"index.jsp\">Back</a>");
    }
}
