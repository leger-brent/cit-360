package Final;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


@WebServlet(name = "accounts", urlPatterns = {"/accounts"})
public class AccountsServlet extends HttpServlet {

    AccountsDAO a = AccountsDAO.getInstance();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
        response.setContentType("text/html");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();

        List<AccountsEntity> accounts = a.ViewAllAccounts();

        if (accounts.size() == 0) {
            output.println("There are no Accounts.<br>");
        }
        for (Iterator i = accounts.iterator(); i.hasNext(); ) {
            AccountsEntity acc = (AccountsEntity) i.next();
            output.println("Account ID: " + acc.getAccountId() + "<br>");
            output.println("Name: " + acc.getAccountNickname() + "<br>");
            output.println("Type: " + acc.getAccountType() + "<br>");
            output.println("Amount: " + acc.getAccountAmount() + "<br>");
            double rate = acc.getAccountInterestRate();
            rate = rate * .01;
            if (rate > 0) {
                output.println("Interest Rate: " + rate + "%<br><br>");
            } else {
                output.println("<br>");
            }

        }
        output.println("<a href=\"index.jsp\">Back</a>");
    }
}