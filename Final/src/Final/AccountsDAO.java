package Final;


import org.hibernate.*;

import java.util.*;


public class AccountsDAO {

    SessionFactory factory = null;
    Session session = null;

    private static AccountsDAO single_instance = null;

    private AccountsDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static AccountsDAO getInstance() {
        if (single_instance == null) {
            single_instance = new AccountsDAO();
        }
        return single_instance;
    }

    public List ViewAllAccounts() {
        Transaction tx = null;
        List<AccountsEntity> a = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            a = session.createQuery("from Final.AccountsEntity").list();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            return a;
        }
    }

    public void addAccount(AccountsEntity a) {
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            session.save(a);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
        }
    }

    public void deleteAccount(int id) {
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            AccountsEntity a = session.get(AccountsEntity.class, id);
            session.delete(a);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
        }
    }

    public AccountsEntity findAccount(int id) {
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            AccountsEntity a = session.get(AccountsEntity.class, id);
            return a;
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        }
    }

    public void updateAmount(int id, long amount) {
        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            AccountsEntity a = session.get(AccountsEntity.class, id);
            long total = amount + a.getAccountAmount();
            a.setAccountAmount(total);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        }
    }

}