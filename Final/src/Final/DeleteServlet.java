package Final;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "delete", urlPatterns = {"/delete"})
public class DeleteServlet extends HttpServlet {

    AccountsDAO a = AccountsDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter output = response.getWriter();

        String id = request.getParameter("id");
        if (id == null || id == "") {
            output.println("You must enter an ID Number");
        } else {
            if (a.findAccount(Integer.parseInt(id)) != null) {
                if (a.findAccount(Integer.parseInt(id)).getAccountAmount() > 0) {
                    output.println("You must withdraw all funds before closing this account<br>");
                } else {
                    a.deleteAccount(Integer.parseInt(id));
                    output.println("Account has been deleted.<br>");
                }
            } else {
                output.println("That account does not exist.<br>");
            }
        }

        output.println("<a href=\"index.jsp\">Back</a>");
    }
}
