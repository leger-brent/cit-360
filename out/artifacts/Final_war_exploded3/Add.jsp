<%--
  Created by IntelliJ IDEA.
  User: Brent
  Date: 7/19/2020
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Account</title>
</head>
<body>
<fieldset>
    <legend>Add Account:</legend>
    <form action="add">
        <label>Type:</label><br>
        <input type="radio" checked name="radiogroup" id="Checking" value="Checking">
        <label for="Checking">Checking</label>
        <input type="radio" name="radiogroup" id="Savings" value="Savings">
        <label for="Savings">Savings</label><br>
        <label for="nickname">Name:</label><br>
        <input type="text" id="nickname" name="nickname"><br>
        <label for="amount">Initial Deposit Amount:</label><br>
        <input type="number" id="amount" name="amount"><br><br>
        <input type="submit" value="Submit"><br><br>
    </form>
</fieldset>

<a href="index.jsp">Home</a><br>
</body>
</html>
