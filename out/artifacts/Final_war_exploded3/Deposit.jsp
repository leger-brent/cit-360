<%--
  Created by IntelliJ IDEA.
  User: Brent
  Date: 7/19/2020
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Deposit or Withdraw</title>
</head>
<body>

<fieldset>
    <legend>Deposit or Withdraw:</legend>
    <form action="deposit">
        <label>Action:</label><br>
        <input type="radio" id="Deposit" checked name="radiogroup" value="Deposit">
        <label for="Deposit">Deposit</label>
        <input type="radio" id="Withdraw" name="radiogroup" value="Withdraw">
        <label for="Withdraw">Withdraw</label><br>
        <label for="id">Account ID:</label><br>
        <input type="number" id="id" name="id"><br>
        <label for="amount">Amount:</label><br>
        <input type="number" id="amount" name="amount"><br><br>
        <input type="submit" value="Submit"><br><br>
    </form>
</fieldset>

<a href="index.jsp">Home</a><br>

</body>
</html>
