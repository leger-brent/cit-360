package JSON;

public class Hero {

    String name;
    String rName = "temp";
    String rElement = "temp";
    int hp;
    int str;
    int def;
    int spd;

    public Hero(String name, int hp, int str, int def, int spd) {
        this.name = name;
        this.hp = hp;
        this.str = str;
        this.def = def;
        this.spd = spd;
    }

    public Hero() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public int getSpd() {
        return spd;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", hp=" + hp +
                ", str=" + str +
                ", def=" + def +
                ", spd=" + spd +
                '}';
    }
}
