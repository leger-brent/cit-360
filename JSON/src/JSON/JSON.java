package JSON;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;
import java.util.ArrayList;

public class JSON {

    //This function accepts an object and uses the mapper to convert it into JSON
    public static String objectToJSON(Hero h) {
        //These variables need to be defined outside the scope of the try block or they can't be returned.
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        //Try/catch provides error handling
        try {
            //the String variable created above is assigned the string value of the object passed to the function
            s = mapper.writeValueAsString(h);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //Return the string to use in the program
        return s;
    }

    //This function accepts a string and uses the mapper to convert it into a hero object
    public static Hero JSONToObject(String s) {
        //These variables need to be defined outside the scope of the try block or they can't be returned.
        ObjectMapper mapper = new ObjectMapper();
        Hero h = null;
        try {
            //The Hero variable is assigned the data from the string as it is mapped in the Hero class.
            h = mapper.readValue(s, Hero.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //Return the object to use in the program
        return h;
    }

    public static void main(String[] args) {
        //Create an ArrayList to hold objects. This will be used to validate we converted objects to JSON and back.
        ArrayList<Hero> heroes = new ArrayList<Hero>();

        //Create an ArrayList to hold JSON strings. This will be used to validate that objects were converted to JSON.
        ArrayList<String> jsonStrings = new ArrayList<>();

        //Create 3 hero objects and add them to the heroes ArrayList.
        heroes.add(0, new Hero("Vahn", 10, 5, 5, 5));
        heroes.add(1, new Hero("Noa", 8, 4, 4, 7));
        heroes.add(2, new Hero("Gala", 11, 6, 6, 3));


        /*This for loop will perform 2 actions. First it will verify that the objects are in the heroes ArrayList.
         * Next it will pass each object to the objectToJSON function to convert it to a string and add it to the
         * jsonStrings ArrayList. */
        System.out.println("\nShowing the initial ArrayList of Hero Objects");
        for (Hero h : heroes) {
            System.out.println(h);
            jsonStrings.add(objectToJSON(h));
        }
        System.out.println("\n");

        /*This for loop will perform 2 actions. First it will verify that the new JSONs strings are in the jsonStrings ArrayList.
         * Next it will pass each string to the JSONToObject function to convert it to an object and add it to the
         * heroes ArrayList. */
        System.out.println("\nShowing the Hero Objects converted into JSON");
        for (String s : jsonStrings) {
            System.out.println(s);
            heroes.add(JSONToObject(s));
        }
        System.out.println("\n");

        //Just to help see the difference, this changes the names of the original three objects in the heroes ArrayList.
        System.out.println("Changing the name of the Original Three Hero Objects....");
        heroes.get(0).setName("Meta");
        heroes.get(1).setName("Terra");
        heroes.get(2).setName("Ozma");

        /*Now that the names of the original three objects are changed, this verifies that there are now six objects in
         * the heroes ArrayList. The last three objects came from converting the original three to JSON and then back into
         * Hero objects.*/
        System.out.println("\nShowing the JSON converted back into Objects and added to the Heroes ArrayList");
        for (Hero h : heroes) {
            System.out.println(h);
        }
    }
}
