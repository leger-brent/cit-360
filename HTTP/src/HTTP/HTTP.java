package HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTP {

    //Create a URL connection variable
    private static HttpURLConnection connection;

    public static void main(String[] args) {

        //Variables for reading the response from the website
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();

        //Doing a for loop to show the difference between a successful request and an error.
        for (int i = 0; i < 2; i++) {

            //Error handling required. Encapsulating in a try/catch block
            try {

                //Create a new URL variable
                URL url;

                //First loop will try a valid URL (at least at the time of this program)
                if (i == 0) {
                    url = new URL("https://pokeapi.co/api/v2/pokemon/1/");

                    //Second loop will try an invalid URL
                } else {
                    url = new URL("https://pokeapi.co/api/v2/pokemon/999/");

                }

                // This section opens the connection and specifies the request as a GET. I set timeouts for 5 seconds.
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);

                // Requests have response codes for validation. This int will read the response code and print it.
                int status = connection.getResponseCode();
                System.out.println(status);

                //Clears the response content so that it only prints the data for the current loop.
                responseContent.delete(0, responseContent.length());

                //if the code is an error, we'll get the error stream.
                if (status > 299) {
                    reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                }

                //if the code is valid, we'll get the input stream
                else {
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                }

                //This will append all the information to our response content
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }

                //close the reader
                reader.close();

                //print out the response content
                System.out.println(responseContent.toString());

                //Error handling
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Close the HTTP connection
            finally {
                connection.disconnect();
            }
        }
    }
}
